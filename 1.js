/*При роботі на різних пристроях, наприклад на смартфоні використання клавіатури може бути недоцільним або неможливим. Також Існує багато альтернативних методів вводу,
таких як голосове керування, яке може бути більш зручними для користувача. Клавіатура надає обмежені можливості. Введення здійснюється шляхом натискання клавіш, що може бути незручним
для користувача і обмежувати його можливості взаємодії з системою.*/

const buttons = document.querySelectorAll('.btn');
const enterButton = document.getElementById('enter');

document.addEventListener('keydown', (event) => {
    const key = event.key.toLowerCase();

    buttons.forEach((button) => {
        const buttonText = button.innerText.toLowerCase();

        if (buttonText === key) {
            button.style.background = 'blue';
        } else {
            button.style.background = 'black';
        }
    });
});